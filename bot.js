// EISPRI-STIC BOT : Gestion des rôles

//Déclarations des modules
const Discord = require('discord.js');
const util = require("util");
const fs = require("fs");
const snekfetch = require("snekfetch");
const ical = require("node-ical");

const client = new Discord.Client({
    disableEveryone: true,
    disabledEvents: ["TYPING_START", "TYPING_STOP"]
});

const conf = require("./conf");
const { config } = require('process');

let prefix = conf.prefix;
let ownerid = conf.ownerid;

let messageID = conf.message_id; // Message en cache : Ajout des rôles


//Lors du démarrage du bot
client.on('ready', () => {
    console.log(`Bot prêt !`);

    // Fetch du message pour tous les channels mappés
    client.channels.forEach(async function(key, value) {
        try {
            await key.fetchMessage(messageID);
            console.log(`Message d'ID ${messageID} trouvé et mis en cache.`);
            return;
        } catch (e) {}
    });
    // client.guilds.get(conf.guild_id).channels.get(conf.channel_id).fetchMessage(messageID); // Mise en cache pour le messageReactionAdd, ancienne version


    /////////////////////////////////////////////
    //                                         //
    // ATTENTION! LES CODES SONT VALABLES QUE  //
    //         POUR LE PREMIER SEMSTRE         //
    //                                         //
    /////////////////////////////////////////////
    
    //config groupe semestre-1 2020
    //L1-info
    conf.edt_groups["mist-cmi-info"] = 4072;
    conf.edt_groups["mist-11"] = 1609,6000;
    conf.edt_groups["mist-12"] = 2251,6003;
    conf.edt_groups["mist-21"] = 1107,6056;
    conf.edt_groups["mist-22"] = 2252,6066;
    conf.edt_groups["mist-31"] = 2866,6089;
    conf.edt_groups["mist-32"] = 4845,6092;

    //L2-info
    conf.edt_groups["l2-info-cmi"] = 1010;
    conf.edt_groups["l2-info-1"] = 1547;
    conf.edt_groups["l2-info-2"] = 3250;
    conf.edt_groups["l2-info-3"] = 1548;
    conf.edt_groups["l2-info-4"] = 2024;
    conf.edt_groups["l2-info-5"] = 2096;

    //L3-info
    conf.edt_groups["l3-info-cmi"] = 4307;
    conf.edt_groups["l3-info-1"] = 4249;
    conf.edt_groups["l3-info-2"] = 4250;
    conf.edt_groups["l3-info-3"] = 2334;

    //M1-info
    conf.edt_groups["m1-info-1"] = 4302;
    conf.edt_groups["m1-info-2"] = 4303;

    //M2-info
    conf.edt_groups["m2-info"] = 7261,7260,4336,4335,6191;

    saveconf();
    console.log("Ajout des filières-groupes ok.");
});

//Lors de la réception d'un message
client.on('message', async msg => {
    
    // Protection anti-bots et anti-DM
    if (msg.author.bot || msg.channel.type === "dm") return;

    
    //------------//
    //****HELP****//
    //------------//

    // Message d'aide
    if (msg.content === prefix + 'help') {

        let owner = await client.fetchUser(conf.ownerid[0], false);
        
        
        let embed = new Discord.RichEmbed();
        embed.setTitle(`UTILISATION DU BOT :`);
        embed.setColor(16751616);

        embed.addField(`Commandes utilisateur (utilisables par tous) :`,
                       `\`${prefix}help\` : Affiche ce message.\n`
                     + `\`${prefix}ping\` : Temps de réponse du bot\n`
                     + `\`${prefix}edt [emploi_du_temps]\` : Liste des emplois du temps accessibles ou affichage de l'emploi du temps passé en paramètre.\n`);

        if (ownerid.includes(msg.author.id) || msg.member.hasPermission("ADMINISTRATOR")) {
            embed.addField(`Commandes administrateur (Permissions Administrateur seulement) :`,
                `\`${prefix}setmessage\` : Initialisation du message de mise en place des rôles.\n`
                + `\`${prefix}setedt [emploi_du_temps] [code_EDT]\` : Ajout, modification et suppression des emplois du temps affichables.\n`
                + `\`${prefix}perms\` : Liste des permissions du bot sur le serveur courant.\n`);
        }

        embed.setFooter(`Développé par ${owner.username}#${owner.discriminator}. Contactez-le en cas de problème.`)

        msg.channel.send(embed);
    }

    //------------//
    //****PING****//
    //------------//

    // ping = Ping du client
    if (msg.content === prefix + 'ping') {
        msg.channel.send(`Pong ! Le ping est de ${Math.round(client.ping)} ms.`).catch(console.log);
    }

    //--------------------------//
    //****MESSAGE AJOUT RÔLE****//
    //--------------------------//

    // setmessage = Reset du message mettant à jour les rôles
    if (msg.content === prefix + 'setmessage') {
        if (ownerid.includes(msg.author.id) || msg.member.hasPermission("ADMINISTRATOR")) {

            let embed = new Discord.RichEmbed();
            embed.setTitle(`ATTRIBUTION DES RÔLES:`);
            embed.setColor(16751616);

            embed.addField(`**IMPORTANT**`,
                  `Merci de bien vouloir changer votre pseudo sur le serveur\n`
                + `et le mettre sous la forme \`Prénom Nom - Pseudo\`.\n\n`
                + `Merci également de bien choisir le rôle qui correspond à votre année et pas un autre.\n`)

            embed.addField(`Cliquez sur une réaction pour obtenir le rôle de votre filière.`,
                  `Vous ne pouvez vous attribuer qu'un rôle à la fois.\n`
                + `:one: - Vous êtes en L1.\n`
                + `:two: - Vous êtes en L2 Info.\n`
                + `:three: - Vous êtes en L2 TRI.\n`
                + `:four: - Vous êtes en L2 ESET.\n`
                + `:five: - Vous êtes en L3 Info.\n`
                + `:six: - Vous êtes en L3 TRI.\n`
                + `:seven: - Vous êtes en L3 ESET.\n`
                + `:eight: - Vous êtes en M1 Info.\n`
                + `:nine: - Vous êtes en M2 Info.\n`
                + `:keycap_ten: - Vous avez déjà passé votre master à l'USMB.\n`
                );

            let message = await msg.channel.send(embed);
            await message.react("1️⃣");
            await message.react("2️⃣");
            await message.react("3️⃣");
            await message.react("4️⃣");
            await message.react("5️⃣");
            await message.react("6️⃣");
            await message.react("7️⃣");
            await message.react("8️⃣");
            await message.react("9️⃣");
            await message.react("🔟");
            
            messageID = message.id;
            conf.message_id = messageID;

            console.log(`Message set à l'ID ${messageID}. Sauvegarde de la database...`);
            saveconf();
        }
    }

    //-----------------------//
    //****EMPLOI DU TEMPS****//
    //-----------------------//

    if (msg.content.startsWith(conf.prefix + "edt")) {
        // Check du type d'edt demandé
        let type = msg.content.split(" ").slice(1,2).join("");
        console.log(type);

        // EDT invalide, on donne les listes d'emploi du temps disponibles
        if (!Object.keys(conf.edt_groups).includes(type)) {
            // Affichage des edt
            let edtlist = Object.keys(conf.edt_groups).join(", ");
            msg.channel.send(`Liste des emplois du temps visionnables :\`\`\`\n${edtlist}\`\`\``);
            return;
        }
        
        // Code de l'EDT
        let code = conf.edt_groups[type];

        // Gestion de la date
        var h = new Date();
        
        // Test si il y a une date en paramètre après la commande
        if(msg.content.split(" ").splice(2,3).join("").match(new RegExp('(([0-2]?[0-9])|3[01])\/(((0?[1-9])|(1[012])))\/[0-9]{4}$'))) {
            let date = msg.content.split(" ").splice(2,3).join("");
            let d = date.split("/");
            // h prend la valeur de la date placé en paramètre
            h = new Date(d[2], d[1]-1, d[0]);
        } else {
            // Pas de date en paramètre, donc on gère les décalages automatiques

            // Changement au jour suivant une fois passé 20h
            h.setHours(h.getHours()+4);
    
            // Changement au lundi suivant si jour = samedi ou dimanche
            if (h.getDay() === 6) {
                // Samedi
                h.setDate(h.getDate() + 2);
            } else if (h.getDay() === 0) {
                // Dimanche
                h.setDate(h.getDate() + 1);
            }
        }
        
        var year = h.getFullYear().toString();
        var month = (h.getMonth()+1).toString().padStart(2, "0");
        var day = h.getDate().toString().padStart(2, "0");

        let link = `https://ade6-usmb-ro.grenet.fr/jsp/custom/modules/plannings/direct_cal.jsp?resources=${code}&projectId=2&calType=ical&login=iCalExport&password=73rosav&firstDate=${year}-${month}-${day}&lastDate=${year}-${month}-${day}`;
        let edt = [];
        ical.fromURL(link, {}, function (err, data) {
            for (let k in data) {
                if (data.hasOwnProperty(k)) {
                    var ev = data[k];
                    if (data[k].type == 'VEVENT') {
                        if (ev.start.getDate() == h.getDate()) {
                            edt.push(ev);
                        }
                    }
                }
            }
            edt.sort(sort_edt);

            // Affichage
            let date = h;

            var embed = {
                "description": `**Emploi du temps du ${day}/${month}/${year} : ${type}**`,
                "color": 4886754,
                "fields": [],
                "footer": {
                    "icon_url": "",
                    "text": `${code === `` ? `Aucun code EDT` : `Code EDT: \"${code}\"`}`
                }
            };

            if (edt.length === 0) {
                // Aucun affichage
                embed.fields.push({
                    "name": "Aucun emploi du temps disponible",
                    "value": `Vous n'avez pas cours, ou une erreur a eu lieu.\n` +
                             `Vérifiez votre EDT sur https://www.univ-smb.fr/edt.`
                });
            } else {
                // Affichage EDT
                let date = h;

                for (i = 0; i < edt.length; i++) {
                    // Nom des groupes
                    let groupes = ""
                    try {
                        groupes = edt[i].description.match(/\n\n((?!B-Ens)(CMI[\s-_]?)?([-_A-Za-z0-9\&]+( G[0-9])?\n))+/g).join("").replace(/\n\n/g, "").replace(/\n/g, ", ").slice(0,-2);
                    } catch (error) {
                        groupes = "INCONNU";
                    }

                    let groupesList = groupes.split(", ");
    
                    // Noms des profs
                    let profList = edt[i].description.replace(/\n\n/g, "").replace(/\n/g, ", ").split(", ");
                    let prof = []
                    for (let i = 0; i < profList.length; i++) {
                        if (!groupesList.includes(profList[i])) {
                            prof.push(profList[i]);
                            
                        }
                    }
                    prof.splice(-2, 2).join(", ");

                    let location = edt[i].location.split("(").slice(0,1).join("");
    
                    embed.fields.push({ 
                        "name": edt[i].start.toLocaleTimeString("fr-FR") + " - " + edt[i].end.toLocaleTimeString("fr-FR"),
                        "value": `__Groupe(s) :__ ${groupes}\n` +
                                 `__Cours :__ ${edt[i].summary}\n` +
                                 `__Salle :__ ${location}\n` +
                                 `__Prof(s) :__ ${prof}\n`
                    });
                }
    
            }

            msg.channel.send({embed});
        });
    }


    // Modification des emplois du temps
    if (msg.content.startsWith(conf.prefix + "setedt")) {
        if (ownerid.includes(msg.author.id) || msg.member.hasPermission("ADMINISTRATOR")) {
            // Check du type d'edt demandé
            let edt_name = msg.content.split(" ").slice(1, 2).join("");
            let edt_code = msg.content.split(" ").slice(2, 3).join("");

            if (edt_name === "") {
                msg.channel.send("Aucun emploi du temps déclaré. Veuillez donner le nom de l'emploi du temps à modifier.");
                return;
            }

            let index_edt = Object.keys(conf.edt_groups).indexOf(edt_name);

            if (edt_code === "") {
                if (index_edt !== -1) {
                    // Suppression
                    let old_code = conf.edt_groups[edt_name];
                    delete conf.edt_groups[edt_name];
                    msg.channel.send(`Emploi du temps \`${edt_name}\` supprimé. (Ancien code EDT: \`${old_code}\`)`);
                }

                else {
                    // Erreur : Suppression d'un EDT inexistant
                    msg.channel.send(`L'emploi du temps \`${edt_name}\` n'existe pas et ne peut par conséquent pas être supprimé.`);
                    return;
                }
            } else {
                if (index_edt !== -1) {
                    // Modification
                    let old_code = conf.edt_groups[edt_name];
                    conf.edt_groups[edt_name] = edt_code;
                    msg.channel.send(`Emploi du temps \`${edt_name}\` modifié. (Ancien code EDT: \`${old_code}\`)`);
                }

                else {
                    // Création
                    conf.edt_groups[edt_name] = edt_code;
                    msg.channel.send(`Emploi du temps \`${edt_name}\` créé.`);
                }
            }

            // Sauvegarde de la base de données.
            saveconf();
        }
    }


    // perms = Permissions du bot dans la guilde
    if (msg.content.startsWith(prefix + 'perms')) {

        if (ownerid.includes(msg.author.id) || msg.member.hasPermission("ADMINISTRATOR")) {
            let permslist = util.inspect(msg.guild.me.permissions.serialize()); //Le util.inspect sort l'object correctement, sinon le message renvoie "object Object"
            let embed = { "color": 4666185, "description": `Voici toutes les permissions qui me sont accordées dans ${msg.guild.name}.\n\`\`\`js\n${permslist}\n\`\`\`` };
            msg.channel.send({ embed }).catch(console.log);
        }
    }


    //-------------//
    //****ADMIN****//
    //-------------//

    // saveconf = Sauvegarde de la config
    if (msg.content === prefix + 'saveconf') {
        if (ownerid.includes(msg.author.id)) {
            let res = saveconf();
            if (res) {
                msg.react("✔");
            } else {
                msg.react("❌");
            }
        }
    }


    // eval = Évalue et exécute du code js (attention, dangereux !!)
    if (conf.ownerid.includes(msg.author.id) && msg.content.startsWith(conf.prefix + "eval")) {
        let code = msg.content.split(' ').slice(1).join(' ');
        if (!code) return;
        let evaled;
        try {
            evaled = eval(code); //Le code est exécuté et le résultat est renvoyé dans evaled
        } catch (e) { //Les erreurs reviennent ici
            msg.channel.send(`Erreur détectée !\n\`\`\`js\n${e}\`\`\``).catch(console.log);
            return;
        }

        if (typeof (evaled) !== "string") { //Si evaled n'est pas un string
            evaled = util.inspect(evaled)
        }

        if (evaled.length > 1991) { //Si le message est trop grand
            msg.channel.send(`Message trop grand.`).catch(console.log);
            return;
        }

        if (!evaled || typeof (evaled) !== "string" || evaled.length === 0 || evaled === "undefined") {
            msg.react("✔").catch(console.log);
            return;
        }

        let re = new RegExp(client.token, "gi"); //regExp pour détecter si on exécute un client.token
        evaled = evaled.replace(re, "token"); //On remplace tous les client.token
        msg.channel.send(`\`\`\`js\n${evaled}\`\`\``).catch(console.log);
    }

});

//-------------------------//
//****GESTION RÉACTIONS****//
//-------------------------//

client.on('messageReactionAdd', async (reaction, user) => {
    if (reaction.message.id !== messageID || user.bot == true) return; // On ne gère que le message voulu, et pas les réactions des botsw

    let emoji = reaction.emoji;
    let userInGuild = await reaction.message.guild.fetchMember(user.id, false); // On récupère l'utilisateur dans la guilde pour l'accès aux rôles

    // ID rôle attribué (pour la suppression des rôles de type "Pas de rôle" ou des autres années)
    let roleAttribue = null;

    // Rôles d'année
    let roleListID = [
        conf.role_id["pas_de_role"], // Pas de rôle
        conf.role_id["l1"], // L1 
        conf.role_id["l2_info"], // L2 Info
        conf.role_id["l2_tri"], // L2 TRI
        conf.role_id["l2_eset"], // L2 ESET
        conf.role_id["l3_info"], // L3 Info
        conf.role_id["l3_tri"], // L3 TRI
        conf.role_id["l3_eset"], // L3 ESET
        conf.role_id["m1_info"], // M1 Info
        conf.role_id["m2_info"], // M2 Info
        conf.role_id["master++"]  // Master ++
    ];

    const name = emoji.name;

    switch (name) {
        case "1️⃣":
            roleAttribue = roleListID[1];
            break;

        case "2️⃣":
            roleAttribue = roleListID[2];
            break;

        case "3️⃣":
            roleAttribue = roleListID[3];
            break;
        
        case "4️⃣":
            roleAttribue = roleListID[4];
            break;
        
        case "5️⃣":
            roleAttribue = roleListID[5];
            break;
        
        case "6️⃣":
            roleAttribue = roleListID[6];
            break;
        
        case "7️⃣":
            roleAttribue = roleListID[7];
            break;

        case "8️⃣":
            roleAttribue = roleListID[8];
            break;

        case "9️⃣":
            roleAttribue = roleListID[9];
            break;

        case "🔟":
            roleAttribue = roleListID[10];
            break;
    
        default:
            break;
    }

    // Si on a trouvé un rôle a attribuer (dans les réactions données par le bot)
    if (roleAttribue !== null) {
        // On enlève tous les rôles dans la liste (sont enlevés que ceux possédés par l'utilisateur)
        await userInGuild.removeRoles(roleListID);

        // On rajoute le rôle choisi
        await userInGuild.addRole(roleAttribue);
    }

    // Enlèvement de la réaction de l'utilisateur, et fin du traitement.
    reaction.remove(user);
});


//-------------//
//****UTILS****//
//-------------//
function saveconf() {
    fs.writeFile('./conf.json', JSON.stringify(conf), (err) => {
        if (err) {
            console.log("Sauvegarde de la database échouée.");
            return false;
        }
    });
    console.log("Sauvegarde de la database effectuée !");
    return true;
}

function sort_edt(date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1.start > date2.start) return 1;
    if (date1.start < date2.start) return -1;
    return 0;
};


// Login du bot
client.login(conf.token);